// EC2
data "aws_ami" "wordpress" {
  most_recent = true
  filter {
    name = "name"
    values = [ "bitnami-wordpress-4.7-0-linux-ubuntu-14.04.3-x86_64-hvm-ebs*" ]
  }
}

resource "aws_key_pair" "kp" {
  key_name   = "acme"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDB8FXYQ7HlvWNUIuT+yZDr/dC3azzsQ45mtyHqitlNynRGpZo+ctCH/ikW/bT7wDmYv4CLg28+uhKW4w4hYpGTj4RSdazxsvTxqOMnWG248WHtU2IbuyeNAbHYUl6XZu4Mx6+Z1jm5W496WESQ1rIgTKEHzcCrxWF9dps/zCH4OAJMoXMbfPqw6T4J7s8wWTOxVCFikwL35szJ1U5YAq1Fo8FL9KCX/nZ+6VwdVJzdDg5oq/2Vzuh0hk19gAOndmww7es6PhBaZFNqpbyBMhS9ovE8lL280cLTKSeuLRddUJ7YvY/pAU1BzWv1jyLHTVej4JYFEGwvELVvsXngdlyx"
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.wordpress.id}"
  instance_type = "t2.micro"
  key_name      = "${aws_key_pair.kp.key_name}"
  subnet_id     = "${element(module.vpc_test.public_subnets, 0)}"
  tags {
    Name = "${var.name}-Wordpress"
  }
}

resource "aws_security_group" "web" {
  name   = "${var.name}-web"
  vpc_id = "${module.vpc_test.vpc}"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["189.39.55.66/32"]
  }
  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      security_groups = ["${lookup(module.test.security_group, "id")}"]
  }
  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "web" {
  instance = "${aws_instance.web.id}"
  vpc      = true
}
