# Create a new application load balancer
resource "aws_lb" "nlb" {
  name                       = "${var.name}"
  internal                   = "${var.internal}"
  enable_deletion_protection = "${var.enable_deletion_protection}"
  subnets                    = ["${var.subnets}"]
  idle_timeout               = "${var.idle_timeout}"

  #enable_deletion_protection = "${var.enable_deletion_protection}"
  tags               = "${var.tags}"
  load_balancer_type = "network"

  #  access_logs {
  #    bucket = "${module.s3_bucket_access_log.id}"
  #    prefix = ""
  access_logs {
    bucket = "${module.s3_bucket_access_log.id}"
    prefix = ""
  }

  #access_logs {
  #  bucket = "${aws_s3_bucket.lb_logs.bucket}"
  #  prefix = ""
  #}
}

output "nlb" {
  value = {
    id         = "${aws_lb.nlb.id}"
    arn        = "${aws_lb.nlb.arn}"
    arn_suffix = "${aws_lb.nlb.arn_suffix}"
    dns_name   = "${aws_lb.nlb.dns_name}"
    zone_id    = "${aws_lb.nlb.zone_id}"
  }
}
